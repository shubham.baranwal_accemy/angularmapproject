import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngMapViewComponent } from './ang-map-view.component';

describe('AngMapViewComponent', () => {
  let component: AngMapViewComponent;
  let fixture: ComponentFixture<AngMapViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngMapViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AngMapViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
