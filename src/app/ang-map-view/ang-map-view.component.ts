import { PinnedAddressModalComponent } from './../pinned-address-modal/pinned-address-modal.component';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-ang-map-view',
  templateUrl: './ang-map-view.component.html',
  styleUrls: ['./ang-map-view.component.css'],

})
 export class AngMapViewComponent implements OnInit {
    lat: number ;
    lng: number ;
    lstmarker = [];
    completedata: any;


   constructor(private dataservice: DataService, public dialog: MatDialog) { }

   // tslint:disable-next-line: typedef
    ngOnInit() {
      // tslint:disable-next-line: deprecation
      this.dataservice.sendGetRequest().subscribe((data: any[]) => {
        this.lstmarker = data ;
      });
    }

    // tslint:disable-next-line: typedef
    clickedMarker(name: string, street: string, suite: string, city: string, zipcode: string) {
      // alert(`clicked the marker: ${name || street || suite || city || zipcode}`);

      this.dialog.open(PinnedAddressModalComponent, {
          data : {namedata: name, streetdata: street, suitedata: suite, citydata: city, zipcodedata: zipcode}
       });
    }
 }
