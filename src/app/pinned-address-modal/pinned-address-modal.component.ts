import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from '../data.service';
// import { PinnedAddressModalComponent } from './../pinned-address-modal/pinned-address-modal.component';


@Component({
  selector: 'app-pinned-address-modal',
  templateUrl: './pinned-address-modal.component.html',
  styleUrls: ['./pinned-address-modal.component.css']
})
export class PinnedAddressModalComponent implements OnInit {
  name: string;
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  // constructor(public dataService: DataService) { }
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<PinnedAddressModalComponent>
  ) {
    this.name = data.namedata;
    this.street = data.streetdata;
    this.suite = data.suitedata;
    this.city = data.citydata;
    this.zipcode = data.zipcodedata;

  }

  // tslint:disable-next-line: typedef
  ngOnInit() {}
}
