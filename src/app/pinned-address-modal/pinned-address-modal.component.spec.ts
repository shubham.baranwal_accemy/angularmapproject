import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PinnedAddressModalComponent } from './pinned-address-modal.component';

describe('PinnedAddressModalComponent', () => {
  let component: PinnedAddressModalComponent;
  let fixture: ComponentFixture<PinnedAddressModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PinnedAddressModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PinnedAddressModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
