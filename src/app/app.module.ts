import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngMapViewComponent } from './ang-map-view/ang-map-view.component';
import { UserListsComponent } from './user-lists/user-lists.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { PinnedAddressModalComponent } from './pinned-address-modal/pinned-address-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    AngMapViewComponent,
    UserListsComponent,
    PinnedAddressModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path: 'ang-map-view', component: AngMapViewComponent},
      {path: 'user-lists', component: UserListsComponent},
    ]),
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDydMQrWFXDqLQVbNMoIiRZckR0E7IyJWY'
    }),
    MatDialogModule,
    NoopAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [PinnedAddressModalComponent]
})
export class AppModule { }
